Are you looking for jumping castles for sale? Then this is the right place to be. Australian inflatables manufactures and distributes commercial grade jumping castles and other inflatable games in Australian market. Our products and services are not limited to Australia. We can ship world wide.

Address: 6 Acorn Close, Wauchope, NSW 2446, Australia

Phone: +61 2 8880 5570

Website: https://www.australian-inflatables.com
